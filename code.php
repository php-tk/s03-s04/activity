<?php

// Objects as Variables

/*$buildingObj = (object) [
	'name' => 'Caswynn Building',
	'floors' => 8,
	'address' => (object) [
		'barangay' => 'Sacred Heart',
		'city' => 'Quezon City',
		'country' => 'Philippines'
	]
];*/

// Objects from Classes

/*class Building {
	// properties
	public $name;
	public $floors;
	public $address;

	// Constructor
	public function __construct($name, $floors, $address){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

	// methods
	public function printName() {
		return "The name of the building is $this->name";
	}
}*/

// Instances
/*$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');
*/

// Inheritance and Polymorphism

/*class Condominium extends Building {

	public function printNam() {
		return "The name of condominium is $this->name";
	}

	// getters (read-only) and setters (write-only)
	public function getName(){
		return $this->name;
	}

	public function setName($name) {
		$this->name = $name;
	}

}*/

// Instances
/*$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');*/


// ACTIVITY

// activity s03
class Person {
	public $firstName;
	public $middleName;
	public $lastName;

	public function __construct($firstName, $middleName, $lastName) {
		$this->firstName = $firstName;
		$this->middleName = $middleName;
		$this->lastName = $lastName;
	}

	public function printName(){
		return "Your full name is $this->firstName $this->middleName $this->lastName.";
	}

};

$person = new Person("Riza May", "Pelayo", "Ayson");
class Developer extends Person {
	public function printName() {
		return "Your name is $this->firstName $this->middleName $this->lastName and you are a developer.";
	}
};

$developer = new Developer("Steven", "Paul", "Jobs"); 
class Engineer extends Person {
	public function printName() {
		return "You are an engineer named $this->firstName $this->middleName $this->lastName.";
	}
};

$engineer = new Engineer("William", "Henry", "Gates III"); 


// activity s04

class Building {
	public $name;
	public $floors;
	public $address; 

	public function __construct($name, $floors, $address) {
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

	public function printName(){
		return "The name of the building is $this->name.";
	}

	public function getName() {
		return $this->name;
	}

	public function setName($name) {
		$this->name = $name;
	}


	public function getFloors() {
		return $this->floors;
	}

	public function setFloors($floors) {
		$this->floors = $floors;
	}

	public function getAddress() {
		return $this->address;
	}

	public function setAddress($address) {
		$this->address = $address;
	}
};

$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');



class Condominium extends Building {
	public function printName() {
		return "The name of the condominium is $this->name.";
	}

	public function getName() {
		return $this->name;
	}

	public function setName($name) {
		$this->name = $name;
	}

	public function getFloors() {
		return $this->floors;
	}

	public function setFloors($floors) {
		$this->floors = $floors;
	}

	public function getAddress() {
		return $this->address;
	}

	public function setAddress($address) {
		$this->address = $address;
	}
};


$condominium = new Condominium("Enzo Condo", 5, "Buendia Avenue, Makati City, Philippines");
















?>