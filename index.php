<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>s3: Classes and Objects, Inheritance and Polymorphism</title>
	</head>
	<body>

	<!-- <h1>Objects from Variable</h1>

		<p><?php echo $buildingObj->name; ?></p>

	<h1>Objects from Classes</h1>

		<pre><?php var_dump($building); ?></pre>

		<pre><?php print_r($building); ?></pre>
 -->

	<!-- 	<h1>Inheritance and Polymorphism</h1> -->
		
			<!-- method of building -->
				<!-- <p><?php print_r($building->printName()); ?></p>

				<p><?php echo($condominium->printName()); ?></p> -->

	<!-- Session 4 Starts Here -->

<!-- 	<h1>Access Modifiers</h1>

		<h2>Building Variables</h2>
		<p><?php echo $building->name; ?></p>

		<h2>Condominium Variables</h2>
		<p><?php echo $condominium->name; ?></p>

	<h1>Encapsulation</h1>
	<p>The name of the condominium is <?php echo $condominium->getName(); ?></p>

	<p>
		<?php $condominium->setName('Enzo Tower'); ?>
	</p>
	<p>
		The name of the condo has been changed to <?php echo $condominium->getName(''); ?>
	</p> -->


<!-- ACTIVITY -->

<h1>Activity s03</h1>
	<h2>Person</h2>
	<p><?php echo($person->printName() )?></p>

	<h2>Developer</h2>
	<p><?php echo($developer->printName() )?></p>

	<h2>Engineer</h2>
	<p><?php echo($engineer->printName() )?></p>


<h1>Activity s04</h1>
	<h2>Building</h2>
	<p>The name of the building is <?php echo $building->getName(); ?>.</p>

	<p>The Caswyn Building has <?php echo $building->getFloors(); ?> floors.</p>
	<p>The Caswyn Building is located at <?php echo $building->getAddress(); ?>.</p>

	<p><?php $building->setName("Caswyn Complex") ?></p>
	<p>The name of the building has been changed to <?php echo $building->getName() ?>.</p>


	<h2>Condominium</h2>
	<p>The name of the condominium is <?php echo $condominium->getName(); ?></p>
	<p>The Enzo Condo has <?php echo $condominium->getFloors(); ?> floors.</p>

	<p>The Caswyn Building is located at <?php echo $condominium->getAddress(); ?>.</p>
	<p><?php $condominium->setName("Enzo Tower") ?></p>
	<p>The name of the condominium has been changed to <?php echo $condominium->getName(); ?>.</p>



	</body>
</html>